'use strict';

import { createObjectCsvWriter as csvWriter } from 'csv-writer';

export class CSVBuilderService {
  constructor(file, path) {
    const filename = file || `exported_csv_${Date.now()}.csv`;
    this.path = `${path}/${filename}`;
  }

  async generate() {
    if(!this.rowData || this.rowData.length < 1) {
      throw 'Unable to generate CSV. There are no row data to export.'
    }
    try {
      await csvWriter({
        fieldDelimiter:';',
        path: this.path,
        header: this.columnData
      }).writeRecords(this.rowData);
      return {error: false};
  
    } catch (error) {
      return {error: error};
    }
  }

  rows(rows = []) {
    if(rows.length > 0) {
      this.columns(rows[0]);
      this.rowData = rows;
    }
    return this;
  }

  setColumn(columnData) {
    this.columnData = columnData;
    return this;
  }

  columns(fields) {
    this.columnData = Object.keys(fields).map(key => {
      return {id: key, title: key};
    });
    return this;
  }
}