'use strict';

import moment from '@helpers/date';
import fs from 'fs';
import { getErrorStack } from '@helpers/util';

const logFile = () => {
  const today = moment().format('DD-MM-YYYY');
  return `${process.env.APP_LOG_PATH}/log-${today}.log`;
};

const logContent = (content) => {
  const time = moment().format('DD-MM-YYYY HH:mm:ss');
  return `[${time}] - ${content}\n`;
};

const appendLog = (content) => {
  const logfile = logFile();
  fs.appendFile(logfile, content, (err) => {
    if (err) throw err;
  });
};

export const Logger = {
  log: (string) => {
    const content = logContent(string);
    appendLog(content);
  },
  error: (error) => {
    const content = logContent(getErrorStack(error));
    appendLog(content);
  
    if(process.env.APP_ENVIRONMENT == 'local') {
      console.log(content);
    }
  }
};