import nodemailer from 'nodemailer';

/**
 * [mailer description]
 *
 * @param   {[type]}       recipientEmail  [recipientEmail description]
 * @param   {[type]}       subject         [subject description]
 * @param   {undefined[]}  attachments     [attachments description]
 * @param   {[type]}       text            [text description]
 * @param   {[type]}       html            [html description]
 * @param   {undefined[]}  cc              [cc description]
 *
 * @return  {[][]}                         [return description]
 */
export const mailer = async ({recipients, subject, attachments = [], text = '', html = '', cc = []}) => {
  try {
    const SMTP_DETAILS = {
      host: process.env.APP_SMTP_HOST,
      port: parseInt(process.env.APP_SMTP_PORT),
      secure: process.env.APP_SMTP_SECURE == true, // true for 465, false for other ports
      auth: {
        user: process.env.APP_SMTP_USER,
        pass: process.env.APP_SMTP_PASS
      },
      tls: {
        rejectUnauthorized: process.env.APP_SMTP_TLS_REJECTUNAUTHORIZED == true
      }
    };
    const transporter = nodemailer.createTransport(SMTP_DETAILS);
    const options = {
      from: process.env.APP_SENDER_EMAIL,
      to: recipients,
      cc: cc,
      subject: subject,
      text: text,
      html: html,
      attachments: attachments
    };
    transporter.sendMail(options, (error, payload) => ({ error, payload }));
  } catch (error) {
    return { error };
  }
};
