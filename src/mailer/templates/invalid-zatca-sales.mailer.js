'use strict';

const moment = require('@helpers/date');
const { mailer } = require('../index');

const htmlContent = ({ sales, subject }) => {
  const content = {
    subject: subject || `Failed Zatca Sales as of ${moment().format('LLLL')} ${process.env.APP_TIMEZONE}`,
    html: ''
  };
  
  let tbody = '';
  for (const item of sales) {
    tbody +=
    `<tr>
      <td>${item.cegidInternalReference}</td>
    </tr>`;
  }

  content.html = `
    <h3>Schedule Tasks:</h3>
    <table border="1" cellpadding="0" cellspacing="0">
    <thead><td>Cegid Internal Reference</td></thead>
    <tbody>${tbody}</tbody>
    </table>`
  ;

  return content;
};

export const invalidZatcaSalesMailer = async ({ sales, recipients, subject }) => {
  const content = htmlContent({ sales, subject });
  try {
    const options = { ...content, recipients, text: '' };
    const { payload, error } = await mailer(options);
    if(error) throw error;
    
    return { payload };
  } catch (error) {
    return { error };
  }
};
