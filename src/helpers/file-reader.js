'use strict';

import fspromises from 'fs/promises';

/**
 * Function go get files to a directory and can filter by extension 
 * 
 * @param {*} path 
 * @param {*} extension 
 * @returns Dirent
 */
export const getFiles = async (path, extension) => {
  try {
    let result = await fspromises.readdir(path, {withFileTypes: true}, 'utf8');
    
    if(extension) {
      result = result.filter(x => x.name.endsWith(`.${extension}`));
    }

    return result;
  } catch (error) {
    console.error(error);
    /**
     * If file not exist or if json content is invalid json.
     * Return empty object
     */
    return [];
  }
};

export const readFile = async (path) => {
  try {
    const result = await fspromises.readFile(path, 'utf8');
    return result;
  } catch (error) {
    return '';
  }
};

export const remove = async (file) => {
  try {
    await fspromises.unlink(file);
  } catch (error) {
    return '';
  }
};
