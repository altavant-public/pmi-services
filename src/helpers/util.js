'use strict';

import { dirname, join } from 'path';

export const stringToJSON = (jsonString) => {
  try {
    return JSON.parse(jsonString);
  } catch (error) {
    return {};
  }
};

export const filenameFromFullPath = (fullPath) => {
  /* eslint-disable */
  return fullPath.replace(/^.*[\\\/]/, '');
  /* eslint-disable */
};

export const isJson = (item) => {
  item = (typeof item !== 'string')
    ? JSON.stringify(item)
    : item;

  try {
    item = JSON.parse(item);
  } catch (e) {
    return false;
  }

  if (typeof item === 'object' && item !== null) {
    return true;
  }

  return false;
};

export const getErrorStack = (error) => {
  if(typeof error == 'object' && 'stack' in error && error.stack) {
    return error.stack.toString();
  }

  return error;
};

export const getError = (error) => {
  try {
    if(typeof error == 'object' && 'message' in error) {
      return error.message;
    }

    return error;
  } catch (error) {
    return error;
  }
};

export const randomStr = (length) => {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
 }
 return result;
};

export const queryValueFormatter = (stringValueSeparatedByComma) => {
  try {
    return stringValueSeparatedByComma.split(',').map(x => {

      // Get null value
      if(x == 'null') {
        return null;
      }
  
      // Get true boolean value
      if(x == 'true') {
        return true;
      }
  
      // Get false boolean value
      if(x == 'false') {
        return false;
      }
    
      return x;
    });
  } catch (error) {
    return [];
  }
};

export const rootDir = () => {
  return join(dirname(__filename), '..', '..');
};

export const isProduction = process.env.APP_ENVIRONMENT === 'production';

export const isStaging = process.env.APP_ENVIRONMENT === 'qa';

export const isDevelopment = !['production', 'qa'].includes(process.env.APP_ENVIRONMENT);