import schema from './queue.schema';
import MongoDBModel from '../../util/mongodb-model';

class Model extends MongoDBModel {
  constructor() {
    super('queue', schema);
  }
}

export const QueueModel = new Model();