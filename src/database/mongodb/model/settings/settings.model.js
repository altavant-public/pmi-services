import schema from './settings.schema';
import MongoDBModel from '../../util/mongodb-model';

import moment from '@helpers/date';
import * as utilHelper from '@helpers/util';

class Model extends MongoDBModel {
  constructor() {
    super('settings', schema);
  }

  generateKey(type, metadata = {}) {
    let key = type;

    if(!utilHelper.isJson(metadata)) return key;

    try {
      for (const field in metadata) {
        if(metadata[field]) {
          key += metadata[field];
        }
      }
    } catch (error) {
      console.log(error);
    }

    return key;
  }

  async getListViewColumns(records) {
    const rows = records.map(x => {
      const row = {
        '_id': x._id,
        'key': x.key,
        'meta': x.meta,
        'type': x.type,
        'updatedAt': moment(x.updatedAt).format('MMM DD, YYYY hh:mm A z'),
        'createdAt': moment(x.createdAt).format('MMM DD, YYYY hh:mm A z')
      };

      return row;
    });

    return rows;
  }
}

export const SettingsModel = new Model();
