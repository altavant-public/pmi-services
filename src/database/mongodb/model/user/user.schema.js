'use strict';

import mongoose from 'mongoose';
import crypto from 'crypto';

// Schema
const schema = mongoose.Schema({
  username: {type: String, unique: true, trim: true, sparse: true, required: true},
  email: {type: String, unique: true, trim: true, sparse: true},
  auths: [{
    authStrategy: {
      required: true,
      type: String,
      enum: [
        'local'
      ],
      trim: true
    },
    authKey: {type: String},
    authToken: {type: String}
  }],
  name: {
    first: {type: String},
    middle: {type: String},
    last: {type: String,}
  },
  systemRole: {
    type: String,
    enum: [
      'user',
      'admin',
      'it',
      'altavant_support',
      'webmaster'
    ],
    required: true,
    trim: true,
    default: 'user'
  },
  token: {
    resetPassword: {type: String}
  },
  meta: {type: Object},
  permissions: [],
  active: {type: Boolean, default: true},
  hash: {type: String},
  salt: {type: String, default: crypto.randomBytes(16).toString('hex')},
  isTemporaryPassword: {type: Boolean, default: true},
  disabled: {type: Boolean, default: false}
}, {
  timestamps: true
});

/**
 * Method to check the entered password is correct or not
 * valid password method checks whether the user
 * password is correct or not
 * It takes the user password from the request 
 * and salt from user database entry
 * It then hashes user password and salt
 * then checks if this generated hash is equal
 * to user's hash in the database or not
 * If the user's hash is equal to generated hash 
 * then the password is correct otherwise not
 */
 schema.methods = {
  validPassword: function(password) {
    return this.hash === this.hashGenerator(password, this.salt);
  },
  hashGenerator: (password, salt) => crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex')
 };

export default schema;