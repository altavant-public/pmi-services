import schema from './user.schema';
import MongoDBModel from '../../util/mongodb-model';

class Model extends MongoDBModel {
  constructor() {
    super('user', schema);
  }

  async register({ username, password, systemRole }) {
    try {
      const newUser = new this.model();
      newUser.username = username;
      newUser.hash = newUser.hashGenerator(password, newUser.salt);

      if(systemRole) {
        newUser.systemRole = systemRole;
      }

      const result = await newUser.save();
      return result;
    } catch (error) {
      return {error: error.message};
    }
  }
}

export const UserModel = new Model();