'use strict';

import mongoose from 'mongoose';

export const mongodbConnect = async (connectionString) => {
  try {
    connectionString = connectionString || process.env.APP_MONGODB_DATABASE;
    console.info(`Connecting to ${connectionString}`);
    await mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true});
    console.info('\u2713', `MongoDB connected to ${connectionString}`);
    return {};
  } catch (error) {
    console.error('\u2717', 'MongoDB connection error:', error);
    return { error };
  }
};