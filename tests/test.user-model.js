'use strict';

import { UserModel } from '@mongodb/model';
import { mongodbConnect } from '@mongodb/util/mongodb-connect';

async function run() {
  await mongodbConnect('mongodb+srv://');
  // register();
  // login();
  // changePass();
}

async function register() {
  try {
    const user = {username: 'altavant', password: '123456', systemRole: 'admin'};
    const result = await UserModel.register(user);
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

async function login() {
  try {
    const username = 'altavant';
    const password = '11111';
    const { payload } = await UserModel.findOne({username}, '', { lean: false });
    const validPass = payload.validPassword(password);

    console.log(validPass);
  } catch (error) {
    console.log(error);
  }
};

async function changePass() {
  try {
    const username = 'altavant';
    const password = '11111';
    const { payload } = await UserModel.findOne({ username }, {salt: 1, username: 1, token: 1}, { lean: false });

    // Check if user exist
    if(!payload) throw 'not_found';

    const updates = {
      'hash': payload.hashGenerator(password, payload.salt),
      'isTemporaryPassword': true,
      'meta.temporaryPasswordGeneratedBy': payload.username
    };

    const result = await UserModel.updateOne({ username }, updates);
    console.log(result);
  } catch (error) {
    console.error(error);
  }
}

run();

